#!/bin/bash

minikube start
kubectl create ns racebooking
kubectl config set-context minikube --namespace=racebooking

eval $(minikube docker-env)

docker build Microservices/Booking/. -t racebooking/booking:1.0.0
docker build Microservices/RaceTrack/. -t racebooking/racetrack:1.0.0
docker build Microservices/IdGenerator/. -t racebooking/idgenerator:1.0.0
docker build Microservices/APIGateway/. -t racebooking/apigateway:1.0.0
docker build Microservices/RaceTrackInitContainerDB/. -t racebooking/racetrack-init:1.0.0
docker build Microservices/BookingInitContainerDB/. -t racebooking/booking-init:1.0.0

cd Deployment

kubectl apply -f ConfigMap/.
kubectl apply -f Secret/.
kubectl apply -f StatefulSet/.
kubectl apply -f Service/.
kubectl apply -f Deployment/.
kubectl apply -f Pod/.
