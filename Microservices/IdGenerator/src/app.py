#!/usr/bin/python
# -*- coding: utf-8 -*-
from http.server import BaseHTTPRequestHandler, HTTPServer
import time
from datetime import datetime
from time import sleep
import json
import socket
import os


hostName = '0.0.0.0'
serverPort = 8000

itemCode = {'booking': 'BK', 'racetrack': 'RT'}

lastcode = ''

replica_UID = os.environ['POD_IP'].split(".")[3].zfill(3)

def log(text):
    print(datetime.now().strftime('[%d/%m/%Y %H:%M:%S:%m]'), text)
    
def buildResponseBodyJSON(status, data):
	jsonData = {} 
	jsonData["status"] = status
	jsonData["data"] = data
	
	return jsonData
    


def generateID(item):
    global lastcode
    date = datetime.now().strftime('%Y%m%d%H%M%S%f')
    code = itemCode[item] + replica_UID + date[2:-3] + '0'
    if lastcode == code:
        first = code[:-1]
        controlcode = int(code[-1]) + 1
        code = first + controlcode
    lastcode = code
    log("Generated new ID: "+code)
    return code


class MyServer(BaseHTTPRequestHandler):

    def do_GET(self):
        if self.path == '/id/booking':
            log("Received new id/booking request")
            self.send_response(200)
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            body = buildResponseBodyJSON(200, generateID('booking'))
            self.wfile.write(bytes(json.dumps(body), 'utf-8'))
        elif self.path == '/id/racetrack':
            log("Received new id/racetrack request")
            self.send_response(200)
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            body = buildResponseBodyJSON(200, generateID('racetrack'))
            self.wfile.write(bytes(json.dumps(body), 'utf-8'))
        else:
            log("Resource not found")
            self.send_response(404)
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            body = buildResponseBodyJSON(404, "Endpoint {} not found".format(self.path))
            self.wfile.write(bytes(json.dumps(body), 'utf-8'))


if __name__ == '__main__':
    webServer = HTTPServer((hostName, serverPort), MyServer)
    print('Server started http://%s:%s' % (hostName, serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print('Server stopped.')

