## EXPOSED ENDPOINT
This microservices listen port 8000 and communicates in JSON format. The available endpoints are:
- **GET /booking** Retrieve all booking
- **GET /booking/{id}** Retrieve single booking by id
- **GET /booking/racetrack/{id}** Retrieve all booking of a specified racetrack
- **POST /booking** Create the resource passing the specified booking as JSON in the body of the request.
- **PUT /booking/{id}** Update the resource specified by passing a JSON in the body with new values.
- **DELETE /booking/{id}** Delete specified booking

###### JSON example

    {
        "name": "Davide Rendina",
        "email": "daviderendina9@gmail.com",
        "bookingDate": "2022-10-01",
        "entranceBooked": "3",
        "raceTrack": {
            "id": "<trackID>"
        }
    }

____
## API USED
This microservices uses two different external services (through their exposed API):
- **qrickit.com** Used for generating QRCodes
- **Sendgrid** Used for sending email
____
## REQUIRED ENVIRONMENT VARIABLES

- **DATABASE_HOSTNAME**: the hostname of the database
- **DATABASE_NAME**: the name of the database
- **DATABASE_PASSWORD**: password for accessing the database
- **DATABASE_USER**: username for accessing the database


- **SENDGRID_API_KEY** API KEY for Sendgrid service
- **ID_GENERATOR_API**: hostname and URI for retrieve ID for new resources
- **RACETRACK_API**: API endpoint for RaceTrack operations
