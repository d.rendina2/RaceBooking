package com.racebooking.notification;

import com.racebooking.model.Booking;
import com.racebooking.model.RaceTrack;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Attachments;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import com.racebooking.services.RaceTrackService;
import com.racebooking.services.Logger;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Base64;

public class Notifier {

    final static String QRCODE_GENERATOR_API = "https://qrickit.com/api/qr.php?qrsize=500&d=";
    final static String SENDGRID_API_KEY = System.getenv("SENDGRID_API_KEY");
    final static String EMAIL_SENDER = "d.rendina2@campus.unimib.it";

    public static boolean notifyBookingViaEmail(Booking booking){
        String base64qrcode = getQRCode(booking.getId());
        if(base64qrcode == null){
            Logger.log("Exception raised during QrCode API call");
            return false;
        }

        RaceTrack rt = RaceTrackService.getRaceTrack(booking.getRaceTrack().getId());
        if(rt == null || rt.getId() == null){
            Logger.log("Cannot retrieve RaceTrack");
            return false;
        }

        String htmlMsgContent = generateMsgHTML(booking, rt);

        return sendEmail(booking.getEmail(), htmlMsgContent, base64qrcode);
    }

    public static boolean notifyBookingDeletedViaEmail(Booking booking){
        RaceTrack rt = RaceTrackService.getRaceTrack(booking.getRaceTrack().getId());
        if(rt == null || rt.getId() == null){
            Logger.log("Cannot retrieve RaceTrack");
            return false;
        }

        String htmlMsgContent = generateDeleteMsgHTML(booking, rt);

        return sendDeleteEmail(booking.getEmail(), htmlMsgContent);
    }

    private static String getQRCode(String code){
        URL url;
        HttpURLConnection httpURLConnection;
        int responseCode;

        try {
            url = new URL(QRCODE_GENERATOR_API + code);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            responseCode = httpURLConnection.getResponseCode();
        } catch (MalformedURLException e) {
            Logger.log("Cannot generate URL");
            return null;
        } catch (ProtocolException e) {
            Logger.log("Raised ProtocolException: "+e.getMessage());
            return null;
        } catch (IOException e) {
            Logger.log("Raised IOException: "+e.getMessage());
            return null;
        }

        if (responseCode == HttpURLConnection.HTTP_OK) {
            ByteArrayOutputStream buffer;
            try {
                buffer = new ByteArrayOutputStream();
                InputStream inStream = httpURLConnection.getInputStream();

                int nRead;
                byte[] data = new byte[16384];

                while ((nRead = inStream.read(data, 0, data.length)) != -1) {
                    buffer.write(data, 0, nRead);
                }
                buffer.flush();
            } catch (IOException e) {
                Logger.log("Raised IOException during building response: "+e.getMessage());
                return null;
            }
            Logger.log("Code retrieved succesfully");

            return Base64.getEncoder().encodeToString(buffer.toByteArray());
        } else {
            Logger.log("GET request not worked");
            return null;
        }
    }

    public static boolean sendEmail(String sendTo, String msg, String base64qrcode) {
        Email from = new Email(EMAIL_SENDER);
        String subject = "Booking confirmed!";
        Email to = new Email(sendTo);
        Content content = new Content("text/html", msg);

        Mail mail = new Mail(from, subject, to, content);

        Attachments attachments2 = new Attachments();
        attachments2.setContent(base64qrcode);
        attachments2.setType("image/png");
        attachments2.setFilename("code.png");
        attachments2.setDisposition("inline");
        attachments2.setContentId("code");
        mail.addAttachments(attachments2);


        SendGrid sg = new SendGrid(SENDGRID_API_KEY);
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);
            Logger.log("Mail request:" + response.getStatusCode() + response.getBody());
        } catch (IOException ex) {
            return false;
        }
        return true;
    }

    public static boolean sendDeleteEmail(String sendTo, String msg) {
        Email from = new Email(EMAIL_SENDER);
        String subject = "Booking deleted successfully";
        Email to = new Email(sendTo);
        Content content = new Content("text/html", msg);

        Mail mail = new Mail(from, subject, to, content);

        SendGrid sg = new SendGrid(SENDGRID_API_KEY);
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);
            Logger.log("Mail request:" + response.getStatusCode() + response.getBody());
        } catch (IOException ex) {
            return false;
        }
        return true;
    }

    private static String generateMsgHTML(Booking booking, RaceTrack raceTrack){
        String msg = "<html> <body style=\"margin:1%\">" +
                "<p><b>Hello @NAME@ </b></p> <br>" +
                "<p>We are glad to confirm that your entrance has been booked correctly. You can use directly this email in order to enter the circuit.</p>" +
                "<div>" +
                "<p style=\"float: left;\"><img src=\"cid:code\" height=\"40%\" width=\"40%\" border=\"1px\"></p>" +
                "<p><br><br><br>" +
                "<b>Name: </b> @NAME@ <br><br>" +
                "<b>Date: </b> @DATE@ <br><br>" +
                "<b>Race track: </b> @RTNAME@ <br><br>" +
                "<b>Address: </b> @RTADDR@ <br><br>" +
                "<b>People: </b> @PEOPLE@ <br><br>" +
                "</p>" +
                "</div>" +
                "<br> " +
                "<div style=\"clear: left;\">" +
                "<br> <p>Thank you for choosing us, have a nice day</p><br><br>" +
                "<p><b>RaceBooking team</b></p>" +
                "</div></body> </html>";

        msg = msg.replace("@NAME@", booking.getName());
        msg = msg.replace("@DATE@", booking.getBookingDate().toString());
        msg = msg.replace("@RTNAME@", raceTrack.getName());
        msg = msg.replace("@RTADDR@", raceTrack.getAddress());
        msg = msg.replace("@PEOPLE@", String.valueOf(booking.getEntranceBooked()));

        return msg;
    }

    private static String generateDeleteMsgHTML(Booking booking, RaceTrack raceTrack){
        String msg = "<html> <body style=\"margin:1%\">" +
                "<p><b>Hello @NAME@ </b></p> <br>" +
                "<p>Your booking has been succesfully deleted!</p>" +
                "<div>" +
                "<p>" +
                "<b>Name: </b> @NAME@ <br><br>" +
                "<b>Date: </b> @DATE@ <br><br>" +
                "<b>Race track: </b> @RTNAME@ <br><br>" +
                "<b>Address: </b> @RTADDR@ <br><br>" +
                "<b>People: </b> @PEOPLE@ <br><br>" +
                "</p>" +
                "</div>" +
                "<br> " +
                "<div style=\"clear: left;\">" +
                "<br><p> We hope to see you soon! </p><br><br>" +
                "<p><b>RaceBooking team</b></p>" +
                "</div></body> </html>";

        msg = msg.replace("@NAME@", booking.getName());
        msg = msg.replace("@DATE@", booking.getBookingDate().toString());
        msg = msg.replace("@RTNAME@", raceTrack.getName());
        msg = msg.replace("@RTADDR@", raceTrack.getAddress());
        msg = msg.replace("@PEOPLE@", String.valueOf(booking.getEntranceBooked()));

        return msg;
    }
}
