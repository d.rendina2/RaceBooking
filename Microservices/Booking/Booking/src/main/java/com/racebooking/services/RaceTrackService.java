package com.racebooking.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.racebooking.model.JSONResponse;
import com.racebooking.model.JSONResponseFactory;
import com.racebooking.model.RaceTrack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class RaceTrackService {

    final static String RACETRACK_ENDPOINT = "http://" + System.getenv("RACETRACK_ENDPOINT");

    public static RaceTrack getRaceTrack(String trackId){
        HttpURLConnection connection = setUpConnection(RACETRACK_ENDPOINT + "/" + trackId);
        if(connection == null) {
            Logger.log("Connection object is null (getRaceTrack)");
            return null;
        }
        int responseCode;
        try {
            responseCode = connection.getResponseCode();
        } catch (IOException e) {
            Logger.log("Raised IOException (getRaceTrack - during get response code): "+e.getMessage());
            return null;
        }

        if (responseCode == HttpURLConnection.HTTP_OK) {
            String response = "";
            try {
                response = buildResponseString(connection.getInputStream());
            } catch (IOException e) {
                Logger.log("Raised IOException (getRaceTrack - building response string): "+e.getMessage());
                return null;
            }
            if (response == null){
                Logger.log("Builded repsonse is null (getRaceTrack)");
                return null;
            }

            JSONResponse jsonResponse = JSONResponseFactory.fromString(response.replace('\'','\"'));
            if(jsonResponse == null)
                return null;

            RaceTrack raceTrack = null;
            try {
                 raceTrack = new ObjectMapper()
                        .readValue(new ObjectMapper().writeValueAsString(jsonResponse.getData()), RaceTrack.class);
            } catch (JsonProcessingException e) {
                Logger.log("Raised JSONProcessingException (getRaceTrack, build RaceTrack): "+e.getMessage());
                Logger.log("  while rying to use String: "+jsonResponse.getData());
                return null;
            }

            return raceTrack;
        } else {
            Logger.log("GET availability request not worked (getRaceTrack)");
            return null;
        }
    }

    private static HttpURLConnection setUpConnection(String stringURL){
        URL url;
        HttpURLConnection httpURLConnection;

        try {
            url = new URL(stringURL);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
        } catch (MalformedURLException e) {
            Logger.log("Cannot generate URL (getRaceTrack): "+ stringURL);
            return null;
        } catch (ProtocolException e) {
            Logger.log("Raised ProtocolException (getRaceTrack): "+e.getMessage());
            return null;
        } catch (IOException e) {
            Logger.log("Raised IOException (getRaceTrack): "+e.getMessage());
            return null;
        }
        return httpURLConnection;
    }

    private static String buildResponseString(InputStream responseIS){
        BufferedReader in;
        StringBuilder response;
        try {
            in = new BufferedReader(new InputStreamReader(responseIS));
            String inputLine;
            response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        } catch (IOException e) {
            Logger.log("Raised IOException during building response (getRaceTrack): "+e.getMessage());
            return null;
        }
        return response.toString();
    }

}
