package com.racebooking.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import java.sql.Timestamp;
import java.time.LocalDate;


public class Booking {

    String id;
    String name;
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    LocalDate bookingDate = null;
    Timestamp timestamp;
    int entranceBooked;
    RaceTrack raceTrack;
    String email;

    public Booking(String id, String name, LocalDate bookingDate, Timestamp timestamp, int entranceBooked, RaceTrack raceTrack, String email) {
        this.id = id;
        this.name = name;
        this.bookingDate = bookingDate;
        this.timestamp = timestamp;
        this.entranceBooked = entranceBooked;
        this.raceTrack = raceTrack;
        this.email = email;
    }

    public Booking (){}

    public LocalDate getBookingDate() {
        return this.bookingDate;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public int getEntranceBooked() {
        return entranceBooked;
    }

    public RaceTrack getRaceTrack() {
        return raceTrack;
    }

    public String getEmail() {
        return email;
    }

    public void setId(String id) {
        if(id != null)
            this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
