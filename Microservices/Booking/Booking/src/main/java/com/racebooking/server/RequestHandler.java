package com.racebooking.server;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.racebooking.model.Booking;
import com.racebooking.model.RaceTrack;
import com.racebooking.notification.Notifier;
import com.racebooking.services.DatabaseHandler;
import com.racebooking.services.IDGeneratorService;
import com.racebooking.services.Logger;
import com.racebooking.services.RaceTrackService;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class RequestHandler {

    static DatabaseHandler dbHandler;

    public static void init(){
        dbHandler = new DatabaseHandler();
    }

    public static Object getAllHandler(Request request, Response response) {
        Logger.log("Received GET request");

        List<Booking> bookingList = dbHandler.getAll();

        if(bookingList == null){
            response.status(HttpStatus.INTERNAL_SERVER_ERROR_500);
            return generateErrorResponseJSON(HttpStatus.INTERNAL_SERVER_ERROR_500, "Cannot retrieve bookings due to SQLException");
        }

        return generateResponseJSON(HttpStatus.OK_200, bookingList);
    }

    public static Object getHandler(Request request, Response response) {
        String id = request.params(":id");
        Logger.log("Received GET request of resource " + id);

        Booking booking = dbHandler.get(id);
        if(booking == null) {
            response.status(HttpStatus.INTERNAL_SERVER_ERROR_500);
            return generateErrorResponseJSON(HttpStatus.INTERNAL_SERVER_ERROR_500, "Cannot retrieve Booking due to a database error: check SQL syntax and database status");
        }
        if(booking.getId() == null) {
            response.status(HttpStatus.NOT_FOUND_404);
            return generateErrorResponseJSON(HttpStatus.NOT_FOUND_404, "Cannot find resource with the specified id:" + id);
        }

        return generateResponseJSON(HttpStatus.OK_200, booking);
    }

    public static Object createHandler(Request request, Response response) {
        Logger.log("Received POST request");

        Booking booking = buildBookingFromRequest(request.body());
        if(booking == null){
            response.status(HttpStatus.BAD_REQUEST_400);
            return generateErrorResponseJSON(HttpStatus.BAD_REQUEST_400, "Cannot generate resource with the JSON data provided as body: "+request.body());
        }

        if(isBookingDatePassed(booking) || booking.getEntranceBooked() <= 0 || stringEmptyOrNull(booking.getName()) || stringEmptyOrNull(booking.getEmail())){
            response.status(HttpStatus.BAD_REQUEST_400);
            return generateErrorResponseJSON(HttpStatus.BAD_REQUEST_400, "Forbidden values provided: data must be after today, is allowed minimum 1 entrance booked and name/email cannot be empty");
        }

        RaceTrack raceTrack = RaceTrackService.getRaceTrack(booking.getRaceTrack().getId());
        if(!checkRaceTrackExists(raceTrack)){
            response.status(HttpStatus.NOT_FOUND_404);
            return generateErrorResponseJSON(HttpStatus.NOT_FOUND_404, "Provided RaceTrack doesn't exists");
        }

        if(!isRaceTrackAvailable(raceTrack, booking)){
            response.status(HttpStatus.BAD_REQUEST_400);
            return generateErrorResponseJSON(HttpStatus.BAD_REQUEST_400, "There aren't enough entrance available for the selected day in the selected track");
        }

        // Generate the ID for the resource
        String bookingID = IDGeneratorService.generateID();
        if(stringEmptyOrNull(bookingID)) {
            response.status(HttpStatus.INTERNAL_SERVER_ERROR_500);
            return generateErrorResponseJSON(HttpStatus.INTERNAL_SERVER_ERROR_500, "Cannot generate an ID for the resource. Check the status of the IDGenerator microservice");
        }

        booking.setTimestamp(Timestamp.valueOf(LocalDateTime.now()));
        booking.setId(bookingID);

        boolean result = dbHandler.create(booking);
        if(result)
            Notifier.notifyBookingViaEmail(booking);

        if(! result){
            response.status(HttpStatus.INTERNAL_SERVER_ERROR_500);
            return generateErrorResponseJSON(HttpStatus.INTERNAL_SERVER_ERROR_500, "Error while parsing result to JSON (trackID "+booking.getId()+")");
        }

        return generateResponseJSON(HttpStatus.OK_200, booking);
    }

    public static Object deleteHandler(Request request, Response response) {
        String id = request.params(":id");
        Logger.log("Received DELETE request of resource " + id);

        Booking booking = dbHandler.get(id);
        if(booking == null) {
            response.status(HttpStatus.INTERNAL_SERVER_ERROR_500);
            return generateErrorResponseJSON(HttpStatus.INTERNAL_SERVER_ERROR_500, "Cannot retrieve Booking due to a database error: check SQL syntax and database status");
        }
        if(booking.getId() == null) {
            response.status(HttpStatus.BAD_REQUEST_400);
            return generateErrorResponseJSON(HttpStatus.BAD_REQUEST_400, "Cannot find resource with the specified id:" + id);
        }

        if (booking.getBookingDate().isBefore(LocalDate.now().plusDays(1))) {
            Logger.log("Operation aborted: trying to delete past booking");
            response.status(HttpStatus.BAD_REQUEST_400);
            return generateErrorResponseJSON(HttpStatus.BAD_REQUEST_400, "Cannot delete past booking");
        }

        boolean result = dbHandler.delete(booking);
        if(! result) {
            response.status(HttpStatus.INTERNAL_SERVER_ERROR_500);
            return generateErrorResponseJSON(HttpStatus.INTERNAL_SERVER_ERROR_500, "Cannot execute SQL command (raised SQLException)");
        } else {
            Notifier.notifyBookingDeletedViaEmail(booking);
        }
        return generateErrorResponseJSON(HttpStatus.OK_200, String.format("Resource %s deleted correctly", booking.getId()));

    }

    public static Object updateHandler(Request request, Response response) {
        String id = request.params(":id");
        Logger.log("Received PUT request on resource " + id);

        Booking updatedBooking = buildBookingFromRequest(request.body());
        if(updatedBooking == null) {
            response.status(HttpStatus.BAD_REQUEST_400);
            return generateErrorResponseJSON(HttpStatus.BAD_REQUEST_400, "Cannot generate resource with the JSON data provided as body: " + request.body());
        }

        Booking actualBooking = dbHandler.get(id);
        if(actualBooking == null) {
            response.status(HttpStatus.INTERNAL_SERVER_ERROR_500);
            return generateErrorResponseJSON(HttpStatus.INTERNAL_SERVER_ERROR_500, "Cannot retrieve Booking due to a database error: check SQL syntax and database status");
        }
        if(actualBooking.getId() == null) {
            response.status(HttpStatus.NOT_FOUND_404);
            return generateErrorResponseJSON(HttpStatus.NOT_FOUND_404, "Cannot find resource with the specified id:" + id);
        }

        if(isBookingDatePassed(actualBooking) || isBookingDatePassed(updatedBooking)) {
            response.status(HttpStatus.BAD_REQUEST_400);
            return generateErrorResponseJSON(HttpStatus.BAD_REQUEST_400, "Cannot update past booking or set past dates");
        }

        if(stringEmptyOrNull(updatedBooking.getName()))
            updatedBooking.setName(actualBooking.getName());

        if(stringEmptyOrNull(updatedBooking.getEmail()))
            updatedBooking.setEmail(actualBooking.getEmail());

        if(updatedBooking.getEntranceBooked() < 1){
            response.status(HttpStatus.BAD_REQUEST_400);
            return generateErrorResponseJSON(HttpStatus.BAD_REQUEST_400, "Cannot set entranceBookes with a value less than 1");
        }

        if(updatedBooking.getRaceTrack() == null || updatedBooking.getRaceTrack().getId() == null){
            Logger.log("Race track not specified");
            response.status(HttpStatus.BAD_REQUEST_400);
            return generateErrorResponseJSON(HttpStatus.BAD_REQUEST_400, "No raceTrack has been specified for the booking");
        }

        RaceTrack raceTrack = RaceTrackService.getRaceTrack(updatedBooking.getRaceTrack().getId());
        if(! checkRaceTrackExists(raceTrack)) {
            response.status(HttpStatus.NOT_FOUND_404);
            return generateErrorResponseJSON(HttpStatus.NOT_FOUND_404, "Provided RaceTrack doesn't exists");
        }
        if(! isRaceTrackAvailable(raceTrack, updatedBooking, actualBooking)) {
            response.status(HttpStatus.BAD_REQUEST_400);
            return generateErrorResponseJSON(HttpStatus.BAD_REQUEST_400, "There aren't enough entrance available for the selected day in the selected track");
        }

        updatedBooking.setId(id);
        updatedBooking.setTimestamp(actualBooking.getTimestamp());

        boolean result = dbHandler.update(updatedBooking);
        if(result)
            Notifier.notifyBookingViaEmail(updatedBooking);

        response.status(result ? 200 : 500);

        return result ?
                generateResponseJSON(HttpStatus.OK_200, updatedBooking) :
                generateErrorResponseJSON(HttpStatus.INTERNAL_SERVER_ERROR_500, "Cannot update booking values (trackID "+updatedBooking.getId()+"). This may due to an SQLException, wrong provided id or JSON parsing Exception");
    }

    public static Object getAllByTrackHandler(Request request, Response response) {
        String id = request.params(":id");
        Logger.log("Received GET request of resource " + id);

        List<Booking> bookingList = dbHandler.getAllByTrack(id);

        if(bookingList == null) {
            response.status(HttpStatus.INTERNAL_SERVER_ERROR_500);
            return generateErrorResponseJSON(HttpStatus.INTERNAL_SERVER_ERROR_500, "Cannot retrieve bookings due to SQLException");
        }

        return generateResponseJSON(HttpStatus.OK_200, bookingList);
    }


    private static boolean checkRaceTrackExists(RaceTrack raceTrack) {
        return raceTrack != null && !stringEmptyOrNull(raceTrack.getId());
    }

    private static boolean isRaceTrackAvailable(RaceTrack raceTrack, Booking booking, Booking oldBooking) {
        int bookingCount = dbHandler.getBookingCountOn(raceTrack, booking.getBookingDate());
        if (bookingCount == -1)
            return false;

        int maxBooking = raceTrack.getMaxDailyBooking();

        if(maxBooking == -1)
            return false;

        return (maxBooking - bookingCount - booking.getEntranceBooked() + oldBooking.getEntranceBooked()) >= 0;
    }

    private static boolean isRaceTrackAvailable(RaceTrack raceTrack, Booking booking) {
        int bookingCount = dbHandler.getBookingCountOn(raceTrack, booking.getBookingDate());
        if (bookingCount == -1)
            return false;

        int maxBooking = raceTrack.getMaxDailyBooking();

        if(maxBooking == -1)
            return false;

        return (maxBooking - bookingCount - booking.getEntranceBooked()) > 0;
    }

    private static String generateErrorResponseJSON(int code, String msg){
        ObjectNode resp = new ObjectMapper().createObjectNode();
        resp.put("status", code);
        resp.put("data",msg);
        return resp.toString();
    }

    private static String generateResponseJSON(int code, List<Booking> bookingList){
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode resp = mapper.createObjectNode();
        resp.put("status", code);
        ArrayNode array = resp.putArray("data");

        for(Booking b : bookingList){
            ObjectNode book = array.addObject();
            book.put("id",b.getId());
            book.put("name", b.getName());
            book.put("entranceBooked", b.getEntranceBooked());
            book.put("bookingDate", b.getBookingDate().toString());
            book.put("timestamp", b.getTimestamp().toString());
            book.put("email", b.getEmail());

            ObjectNode raceTrack = book.putObject("raceTrack");
            raceTrack.put("id", b.getRaceTrack().getId());
        }
        return resp.toString();
    }

    private static String generateResponseJSON(int code, Booking booking){
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode resp = mapper.createObjectNode();
        resp.put("status", code);
        ObjectNode book = resp.putObject("data");

        book.put("id",booking.getId());
        book.put("name", booking.getName());
        book.put("entranceBooked", booking.getEntranceBooked());
        book.put("bookingDate", booking.getBookingDate().toString());
        book.put("timestamp", booking.getTimestamp().toString());
        book.put("email", booking.getEmail());

        ObjectNode raceTrack = book.putObject("raceTrack");
        raceTrack.put("id", booking.getRaceTrack().getId());

        return resp.toString();
    }

    private static Booking buildBookingFromRequest(String requestBody){
        try {
            if(requestBody.equals("{}"))
                return null;
            else
                return new ObjectMapper()
                        .registerModule(new JavaTimeModule())
                        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                        .readValue(requestBody.replace('\'', '\"'), Booking.class);
        } catch (JsonProcessingException e) {
            Logger.log("Exception while parsing received body: " + e.getMessage());
            return null;
        }
    }

    private static boolean isBookingDatePassed(Booking booking){
        if(LocalDate.now().isAfter(booking.getBookingDate())){
            Logger.log("Operation aborted: inserted date for the booking is in the past");
            return true;
        }
        return false;
    }

    private static boolean stringEmptyOrNull(String st){
        return st == null || st.equals("");
    }
}
