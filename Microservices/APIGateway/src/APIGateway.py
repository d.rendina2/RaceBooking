import hug
import requests
import os
import json
from datetime import datetime


booking_microservice_endpoint = "http://" + os.environ['BOOKING_ENDPOINT']
racetrack_microservice_endpoint = "http://" + os.environ['RACETRACK_ENDPOINT']

LISTEN_PORT = 8000

headers = {"Content-Type": "application/json"}

def log(text):
    print(datetime.now().strftime("[%d/%m/%Y %H:%M:%S:%m]"),text)


log("Microservices connected:\n Booking: {} \n Racetrack: {}".format(booking_microservice_endpoint, racetrack_microservice_endpoint))


#################
##   BOOKING   ##
#################

@hug.get("/booking")
def getAllBooking(response):
    log("GET ALL booking")
    resp = requests.get(booking_microservice_endpoint)
    response.status_code = resp.status_code
    return resp.content

@hug.get("/booking/{id}")
def getBooking(id, response):
    log("GET booking: "+id)
    resp = requests.get(booking_microservice_endpoint + "/" + id)
    response.status_code = resp.status_code
    return resp.content

@hug.get("/booking/racetrack/{id}")
def getBooking(id, response):
    log("GET booking/racetrack: " + id)
    resp = requests.get(booking_microservice_endpoint + "/racetrack/" + id)
    response.status_code = resp.status_code
    return resp.content

@hug.delete("/booking/{id}")
def deleteBooking(id, response):
    log("DELETE booking: "+id)
    resp = requests.delete(booking_microservice_endpoint + "/" + id)
    response.status_code = resp.status_code
    return resp.content

@hug.post("/booking")
def createBooking(body, response):
    log("POST booking")
    log("POST request body: "+ str(body))
    resp = requests.post(booking_microservice_endpoint, json=body, headers=headers)
    response.status_code = resp.status_code
    return resp.content

@hug.put("/booking/{id}")
def putBooking(id, body, response):
    log("PUT booking")
    log("PUT request body: " + str(body))
    resp = requests.put(booking_microservice_endpoint + "/" + id, json=body, headers=headers)
    response.status_code = resp.status_code
    return resp.content


#################
##  RACETRACK  ##
#################

@hug.get("/racetrack")
def getAllRaceTrack(response):
    log("GET ALL racetrack")
    resp = requests.get(racetrack_microservice_endpoint)
    response.status_code = resp.status_code
    return resp.content

@hug.get("/racetrack/{id}")
def getRaceTrack(id, response):
    log("GET racetrack: "+id)
    resp = requests.get(racetrack_microservice_endpoint + "/" + id)
    response.status_code = resp.status_code
    return resp.content

@hug.delete("/racetrack/{id}")
def deleteRaceTrack(id, response):
    log("DELETE racetrack: "+id)
    resp = requests.delete(racetrack_microservice_endpoint + "/" + id)
    response.status_code = resp.status_code
    return resp.content

@hug.post("/racetrack")
def createRaceTrack(body, response):
    log("POST racetrack")
    log("POST request body: "+ str(body))
    resp = requests.post(racetrack_microservice_endpoint, json=body, headers=headers)
    response.status_code = resp.status_code
    return resp.content

@hug.put("/racetrack/{id}")
def putRaceTrack(id, body, response):
    log("PUT racetrack")
    log("PUT request body: " + str(body))
    resp = requests.put(racetrack_microservice_endpoint + "/" + id, json=body, headers=headers)
    response.status_code = resp.status_code
    return resp.content

@hug.not_found()
def not_found_handler():
    log("Endpoint not found")
    return '{"status":404, "data":"The endpoint doesn\'t exists"}'



hug.API(__name__).http.serve(port=LISTEN_PORT, host="0.0.0.0")
