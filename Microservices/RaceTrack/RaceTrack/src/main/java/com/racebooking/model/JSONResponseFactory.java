package com.racebooking.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.racebooking.services.logging.Logger;

public class JSONResponseFactory {

    public static JSONResponse fromString(String str){
        try {
            return new ObjectMapper()
                    .enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
                    .enable(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT)
                    .readValue(str.replace('\'','\"'), JSONResponse.class);
        } catch (JsonProcessingException e) {
            Logger.log("Raised JSONProcessingException (getRaceTrack, build JSONResponse): "+e.getMessage());
            return null;
        }
    }
}
