package com.racebooking.services.database;

import com.racebooking.model.RaceTrack;
import com.racebooking.services.logging.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class DatabaseHandler {

    static String databaseHostname = System.getenv("DATABASE_HOSTNAME");
    static String databaseName = System.getenv("DATABASE_NAME");
    static String databasePassword = System.getenv("DATABASE_PASSWORD");
    static String databaseUser = System.getenv("DATABASE_USER");

    Connection dbconn;
    boolean connectionStatus = false;

    public DatabaseHandler() {
        connectToDB();
    }

    public void connectToDB() {
        int remainingAttempt = 10;
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex){
            Logger.log("Exception raised while searching for jdbc class driver when connecting to DB");
            System.out.println(ex.getMessage());
        }

        String url = "jdbc:mysql://" + databaseHostname + "/" + databaseName;
        Logger.log("Trying to connect with the url: "+url);

        while(dbconn == null){
            try {
                if(remainingAttempt > 0){
                    dbconn = DriverManager.getConnection(url, databaseUser, databasePassword);
                } else {
                    Logger.log("DB Connection status: cannot connect to the database");
                    break;
                }

            } catch (SQLException ex) {
                Logger.log("Cannot connect to DB: "+ex.getMessage());
                Logger.log(" Remaining attempts: " + remainingAttempt);
                try {
                    Thread.sleep(10000);
                    remainingAttempt--;
                } catch (InterruptedException e) {Logger.log("Raised InterruptedException while thread sleep. Remaining attempts: " + remainingAttempt);}
            }
        }

        if(remainingAttempt > 0){
            connectionStatus = true;
            Logger.log("DB Connection status: OK");
        }
    }

    public RaceTrack get(String id){
        RaceTrack track = new RaceTrack();

        try {
            String query = "select * from RaceTrack  where id = ?";
            PreparedStatement preparedStatement = dbconn.prepareStatement(query);
            preparedStatement.setString(1, id);
            ResultSet rs = preparedStatement.executeQuery();

            if(rs.next()) {
                track = new RaceTrack(
                        rs.getString("id"),
                        rs.getString("name"),
                        rs.getString("address"),
                        rs.getInt("lengthInMeters"),
                        rs.getInt("maxDailyBooking")
                );
                Logger.log(String.format("Retrieved RaceTrack %s from database.", id));
            } else {
                Logger.log(String.format("Cannot find RaceTrack %s", id));
            }

        } catch (SQLException ex) {
            Logger.log("SQLException raised: " + ex.getMessage());
            return null;
        }
        return track;
    }

    public List<RaceTrack> getAll(){
        String query = "Select * from RaceTrack";
        List<RaceTrack> trackList = new ArrayList<>();
        try {
            ResultSet rs = dbconn.createStatement().executeQuery(query);
            while(rs.next()){
                RaceTrack track = new RaceTrack(
                        rs.getString("id"),
                        rs.getString("name"),
                        rs.getString("address"),
                        rs.getInt("lengthInMeters"),
                        rs.getInt("maxDailyBooking")
                );
                trackList.add(track);
            }
        } catch (SQLException ex){
            Logger.log("SQLException raised: "+ex.getMessage());
            return null;
        }

        Logger.log("Retrieved result from database. Total tracks retrieved: " + trackList.size());

        return trackList;
    }

    public boolean delete(String id){
        RaceTrack track = this.get(id);

        if(track == null){
            Logger.log("Resource not found: ID "+id);
            return false;
        } else {
            try {
                PreparedStatement preparedStatement = dbconn.prepareStatement("DELETE from RaceTrack where id = ?");
                preparedStatement.setString(1, id);

                preparedStatement.execute();
                boolean result = preparedStatement.getUpdateCount() > 0;
                Logger.log(result ? "Resource deleted correctly" : "No resource has been deleted.");
                return result;
            } catch (SQLException ex){
                Logger.log("Cannot delete resource "+id);
                return false;
            }
        }

    }

    public boolean create(RaceTrack track){
        String insert = "INSERT INTO RaceTrack (id, name, address, lengthInMeters, maxDailyBooking) VALUES (?, ?, ?, ?, ?)";

        try {
            PreparedStatement preparedStatement = dbconn.prepareStatement(insert);
            preparedStatement.setString(1, track.getId());
            preparedStatement.setString(2, track.getName());
            preparedStatement.setString(3, track.getAddress());
            preparedStatement.setString(4, String.valueOf(track.getLengthInMeters()));
            preparedStatement.setString(5, String.valueOf(track.getMaxDailyBooking()));

            int result = preparedStatement.executeUpdate();
            Logger.log(result != -1 ? "Track inserted correctly with id " + track.getId() : "Track has not been inserted");

            return result > 0;

        } catch (SQLException e) {
            Logger.log("Raised SQLException while inserting new track: "+e.getMessage());
            return false;
        }
    }

    public boolean update(RaceTrack updatedTrack){
        try{
            PreparedStatement preparedStatement = dbconn.prepareStatement("UPDATE RaceTrack set name=?, maxDailyBooking=?, lengthInMeters=? where id = ?");
            preparedStatement.setString(1, updatedTrack.getName());
            preparedStatement.setString(2, String.valueOf(updatedTrack.getMaxDailyBooking()));
            preparedStatement.setString(3, String.valueOf(updatedTrack.getLengthInMeters()));
            preparedStatement.setString(4, String.valueOf(updatedTrack.getId()));
            return preparedStatement.executeUpdate() > 0;
        } catch (SQLException ex){
            Logger.log("SQLException raised: "+ex.getMessage());
            return false;
        }
    }

}
