package com.racebooking.services;

import com.fasterxml.jackson.databind.JavaType;
import com.racebooking.model.Booking;
import com.racebooking.model.JSONResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.racebooking.model.JSONResponseFactory;
import com.racebooking.model.RaceTrack;
import com.racebooking.services.logging.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class BookingService {

    final static String GET_BOOKING_TRACK_URL = "http://" + System.getenv("BOOKING_RACETRACK_ENDPOINT");

    public static List<Booking> getAllBookingByTrackId(String trackId){
        HttpURLConnection connection = setUpConnection(GET_BOOKING_TRACK_URL + "/" + trackId);

        if(connection == null){
            Logger.log("Connection object is null (BookingService)");
            return null;
        }
        int responseCode;
        try {
            responseCode = connection.getResponseCode();
        } catch (IOException e) {
            Logger.log("Raised IOException (BookingService, while retrieving response code): "+e.getMessage());
            return null;
        }

        if (responseCode == HttpURLConnection.HTTP_OK) {
            String response;
            try {
                response = buildResponseString(connection.getInputStream());
            } catch (IOException e) {
                Logger.log("Raised IOException (BookingService, while building response): "+e.getMessage());
                return null;
            }
            if (response == null){
                Logger.log("Cannot build response string (BookingService)");
                return null;
            }

            JSONResponse jsonResponse = JSONResponseFactory.fromString(response.replace("\'","\""));
            if(jsonResponse == null)
                return null;
            if(jsonResponse.getData() == null)
                return new ArrayList<>();

            List<Booking> bookingList = new ArrayList<>();
            try {
                for(Object book : (ArrayList)jsonResponse.getData()){
                    Booking b = new ObjectMapper()
                            .readValue(new ObjectMapper().writeValueAsString(book), Booking.class);
                    bookingList.add(b);
                }
            } catch (JsonProcessingException e) {
                Logger.log("Raised JSONProcessingException (BookingService, build booking list): "+e.getMessage());
                return null;
            }

            return bookingList;
        } else {
            Logger.log("GET availability request not worked (BookingService)");
            return null;
        }
    }

    private static HttpURLConnection setUpConnection(String stringURL){
        URL url;
        HttpURLConnection httpURLConnection;

        try {
            url = new URL(stringURL);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
        } catch (MalformedURLException e) {
            Logger.log("Cannot generate URL (BookingService): "+ stringURL);
            Logger.log("Raised MalformedURLException (BookingService): "+e.getMessage());
            return null;
        } catch (ProtocolException e) {
            Logger.log("Raised ProtocolException (BookingService): "+e.getMessage());
            return null;
        } catch (IOException e) {
            Logger.log("Raised IOException (BookingService): "+e.getMessage());
            return null;
        }
        return httpURLConnection;
    }

    private static String buildResponseString(InputStream responseIS){
        BufferedReader in;
        StringBuilder response;
        try {
            in = new BufferedReader(new InputStreamReader(responseIS));
            String inputLine;
            response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        } catch (IOException e) {
            Logger.log("Raised IOException during building response (BookingService): "+e.getMessage());
            return null;
        }
        return response.toString();
    }

}
