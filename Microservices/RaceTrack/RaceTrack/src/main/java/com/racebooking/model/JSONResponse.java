package com.racebooking.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.eclipse.jetty.http.HttpStatus;

public class JSONResponse {
    @JsonProperty("status")
    String status;
    @JsonProperty("data")
    Object data;

    public JSONResponse(){}

    public boolean isResponseOk(){
        return status.equals(String.valueOf(HttpStatus.OK_200));
    }

    public Object getData() {
        return data;
    }

    public String getStatus() {
        return status;
    }

}
