package com.racebooking;

import com.racebooking.server.RequestHandler;
import com.racebooking.services.logging.Logger;

import static spark.Spark.*;

public class Main {

    static int listeningPort = 8000;

    public static void main(String[] args) {

        Logger.log("Listening server on localhost:" + listeningPort);
        port(listeningPort);
        RequestHandler.init();

        // Get all resources
        get("/racetrack", RequestHandler::getAllHandler);

        // Get single resource
        get("/racetrack/:id", RequestHandler::getHandler);

        // Create resource
        post("/racetrack", RequestHandler::createHandler);

        // Update resource
        put("/racetrack/:id",  RequestHandler::updateHandler);

        // Delete resource
        delete("/racetrack/:id", RequestHandler::deleteHandler);
    }

}
