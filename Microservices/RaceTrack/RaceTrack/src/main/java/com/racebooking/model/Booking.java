package com.racebooking.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import java.time.LocalDate;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Booking {
    @JsonProperty("id")
    String id;
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonProperty("bookingDate")
    LocalDate bookingDate = null;
    @JsonProperty("entranceBooked")
    int entranceBooked;

    public int getEntranceBooked() {
        return entranceBooked;
    }

    public LocalDate getBookingDate() {
        return bookingDate;
    }

    public String getId() {
        return id;
    }

    public String getBookingDateAsString(){
        return bookingDate.toString();
    }
}
