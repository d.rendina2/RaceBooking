package com.racebooking.model;

public class RaceTrack {

    String id;
    String name;
    String address;
    int lengthInMeters;
    int maxDailyBooking; // Max number of people in the circuit

    public RaceTrack(){}

    public RaceTrack(String id, String name, String address, int lengthInMeters, int maxDailyBooking) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.lengthInMeters = lengthInMeters;
        this.maxDailyBooking = maxDailyBooking;
    }

    public String getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public int getLengthInMeters() {
        return lengthInMeters;
    }

    public int getMaxDailyBooking() {
        return maxDailyBooking;
    }

    public void setId(String id) {
        this.id = id;
    }
}
