This is the init container for initialize the Booking database.

# ENVIRONMENT VARIABLES

- **MYSQL_ROOT** e **MYSQL_ROOT_PASSWORD**: credentials for accessing database as a user
- **MYSQL_USER** e **MYSQL_USER_PASSWORD**: credentials for creating a new database user used by microservices
- **MYSQL_HOST**: hostname of the database
- **MYSQL_DATABASE**: name of the database
- **MYSQL_TABLE**: table of the database to use

