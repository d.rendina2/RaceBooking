import commands as c
import sys
import json

api_gateway_url = sys.argv[1]
api_bk = api_gateway_url + "/booking"
api_rt = api_gateway_url + "/racetrack"


print("Testcase 1: Add RaceTrack and Booking test. At the end, delete all \n")

rt1 = c.CREATE_racetrack(api_rt, {'name':'Autodromo di Monza', 'address':'Via del Parco 43', 'lengthInMeters':'5793','maxDailyBooking':'500'});
rt2 = c.CREATE_racetrack(api_rt, {'name':'Autodromo di Imola', 'address':'Via Ferrari 1', 'lengthInMeters':'5000','maxDailyBooking':'300'});

print("POST racetrack - Created 2 RaceTrack:")
print("1) '{}' - ID {} (status: {}))".format(rt1["data"]["name"], rt1["data"]["id"], rt1["status"]))
print("2) '{}' - ID {} (status: {}))".format(rt2["data"]["name"], rt2["data"]["id"], rt2["status"]))
print()
input("Press any key to continue..\n\n")

data = {'name':'Davide Rendina 1','email':'daviderendina9@gmail.com','bookingDate':'2023-10-01','entranceBooked':'3','raceTrack':{'id':'null'}}
data["raceTrack"]["id"] = rt1["data"]["id"]
bk1 = c.CREATE_booking(api_bk, data)

data = {'name':'Davide Rendina 2','email':'daviderendina9@gmail.com','bookingDate':'2022-10-01','entranceBooked':'3','raceTrack':{'id':'null'}}
data["raceTrack"]["id"] = rt1["data"]["id"]
bk2 = c.CREATE_booking(api_bk, data)

data = {'name':'Davide Rendina 3','email':'daviderendina9@gmail.com','bookingDate':'2022-10-01','entranceBooked':'3','raceTrack':{'id':'null'}}
data["raceTrack"]["id"] = rt2["data"]["id"]
bk3 = c.CREATE_booking(api_bk, data)

print('POST booking - Created 3 Booking:')
print('1) "{}" for track "{}" (status: {})'.format(bk1["data"]["name"], bk1["data"]["raceTrack"]["id"], bk1["status"]))
print('2) "{}" for track "{}" (status: {})'.format(bk2["data"]["name"], bk2["data"]["raceTrack"]["id"], bk2["status"]))
print('3) "{}" for track "{}" (status: {})'.format(bk3["data"]["name"], bk3["data"]["raceTrack"]["id"], bk3["status"]))
print()
input("Press any key to continue..\n\n")

rt_a = c.GET_allracetrack(api_rt)
print("GET racetrack - All racetrack in DB: (status: ",rt_a["status"],")")
print(json.dumps(rt_a["data"], indent=4))
print()
input("Press any key to continue..\n\n")

bk_a = c.GET_allbooking(api_bk)
print("GET booking - All booking in DB: (status: ",bk_a["status"],")")
print(json.dumps(bk_a["data"], indent=4))
print()
input("Press any key to continue..\n\n")

print("Delete everything inserted")

print("DELETE booking",bk1["data"]["id"],"- Status:", bk1["status"])
bk1 = c.DELETE_booking(api_bk, bk1["data"]["id"])
print("DELETE booking",bk2["data"]["id"],"- Status:", bk2["status"])
bk2 = c.DELETE_booking(api_bk, bk2["data"]["id"])
print("DELETE booking",bk3["data"]["id"],"- Status:", bk3["status"])
bk3 = c.DELETE_booking(api_bk, bk3["data"]["id"])
print("DELETE racetrack",rt1["data"]["id"],"- Status:", rt1["status"])
rt1 = c.DELETE_racetrack(api_rt, rt1["data"]["id"])
print("DELETE racetrack",rt2["data"]["id"],"- Status:", rt2["status"])
rt2 = c.DELETE_racetrack(api_rt, rt2["data"]["id"])
print()
input("Press any key to continue..\n\n")

rt_a = c.GET_allracetrack(api_rt)
print("GET racetrack - All racetrack in DB: (status: ",rt_a["status"],")")
print(json.dumps(rt_a["data"], indent=4))
print()

print()
bk_a = c.GET_allbooking(api_bk)
print("GET booking - All booking in DB: (status: ",bk_a["status"],")")
print(json.dumps(bk_a["data"], indent=4))
print()
print()



print()
print("Don't forget to check your inbox!")
