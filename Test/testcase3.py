import commands as c
import sys
import json

api_gateway_url = sys.argv[1]
api_bk = api_gateway_url + "/booking"
api_rt = api_gateway_url + "/racetrack"


print("--- TESTCASE 3 ---")
print()
print()

print("POST racetrack - A new RT is created with maxBooking=50")
rt1 = c.CREATE_racetrack(api_rt, {'name':'Wrong name', 'address':'Via Adriatica 58', 'lengthInMeters':'2500','maxDailyBooking':'50'});
trackID=rt1["data"]["id"]
trackName = rt1["data"]["name"]
print(json.dumps(rt1["data"], indent=4))
print()
input("Press any key to continue..\n\n")

print("PUT racetrack - correct wrong name")
rt3 = c.MODIFY_racetrack(api_rt, trackID, {'name':'MWC Marco Simoncelli', 'address':'Via Adriatica 58','lengthInMeters':'2500','maxDailyBooking':'50'})
print(json.dumps(rt3["data"], indent=4))
print()
input("Press any key to continue..\n\n")

print("POST booking - A new booking is created with entranceBooked=45")
data = {'name':'Davide Rendina TEST2','email':'daviderendina9@gmail.com','bookingDate':'2023-10-02','entranceBooked':'45','raceTrack':{'id':'null'}}
data["raceTrack"]["id"] = trackID
bk1 = c.CREATE_booking(api_bk, data)
bkID = bk1["data"]["id"]
print(json.dumps(bk1["data"], indent=4))
print()
input("Press any key to continue..\n\n")

print("PUT racetrack - Try to modify RT and change maxDailyBooking to 40.")
rt2 = c.MODIFY_racetrack(api_rt, trackID, {'name':'MWC Marco Simoncelli', 'address':'Via Adriatica 58','lengthInMeters':'2500','maxDailyBooking':'40'})
print("ERROR",rt2["status"],"-", rt2["data"])
print()
input("Press any key to continue..\n\n")

print("DELETE racetrack - Try to delete RT, but there are booking associated")
rt2 = c.DELETE_booking(api_rt, trackID)
print("ERROR",rt2["status"],"-", rt2["data"])
print()
