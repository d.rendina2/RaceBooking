import commands as c
import sys
import json

api_gateway_url = sys.argv[1]
api_bk = api_gateway_url + "/booking"
api_rt = api_gateway_url + "/racetrack"

def printError(r):
    print("ERROR",r["status"],"-", r["data"])
    print()



print("--- TESTCASE 4 ---")
print()
print()

print("GET racetrack")
printError(c.GET_allracetrack(api_rt+"err"))

print("GET booking")
printError(c.GET_allbooking(api_bk+"err"))

print("GET racetrack/:id")
printError(c.GET_racetrack(api_rt, "1234"))

print("GET booking/:id")
printError(c.GET_booking(api_bk, "1234"))

print("DELETE booking")
printError(c.DELETE_booking(api_bk, "1"))

print("DELETE racetrack")
printError(c.DELETE_racetrack(api_rt, "1"))

print("POST booking")
printError(c.CREATE_booking(api_bk, {}))

print("POST racetrack")
printError(c.CREATE_racetrack(api_rt, {}))

print("PUT booking")
printError(c.MODIFY_booking(api_bk, "1", {}))

print("PUT raceTrack")
printError(c.MODIFY_racetrack(api_rt, "1", {}))
